# Сборщик проектов KDX
###Установка:
```bash
npm i
```
или
```bash
yarn
```

затем



```
node ./node_modules/kdx-builder/index.js 
```

Во время установки будут созданы файл конфигурации и скрипты для `npm`
###Конфиг сборщика
Настройки для сборщика находятся в файле kdx.config.json, структура:

kdx.config.json [default]:
```js
{
  "default": { // Настройки по умолчанию
    "paths": { // Пути для сборки
      "sourceRoot": "src/",
      "pages": "src/pages/",
      "output": "build/",
      "publicPath": "/",
      "assets": {
        "js": "js/",
        "css": "css/",
        "fonts": "fonts/",
        "images": "images/",
        "templates": ""
      },
      "resolve": {
        // Тут можно задать алиасы для часто используемых файлов и директорий
        //"someAlias": "./src/some/path/to/some/nested/folder" // Далее, где необхоимо, вызываем алиас вместо пути к файлу @import "someAlias";
      },
      "hashes": false //все изображения, шрифты итд будут сохрянаться с hash
    },
    "kdxTools": false, // Меню, сетка и прочие фичи, полезные при разработке
    "devServer": { 
      "active": false, // По-умолчанию сервер не стартует
      "port": 8080 // Порт сервера, который можно переопределить ниже
    },
    "minify": false, // Минификация
    "provide": {
      // Добавление переменной в js
      //"name": "Homer" // Далее в любом js файле используем  console.log(name); 
    }
  },
  "development": { // Задача по умолчанию, запуск через npm run start
    "kdxTools": true, // Включаем dev-меню, сетку
    "devServer": {
      "active": true // Запускаем сервер
    }
  },
  "test": { // Задача, переопределяющая настройки по умолчанию, запуск через переменную NODE_ENV=test
    "paths": {
      "publicPath": "/"
    },
    "kdxTools": true
  },
  "production": { // Задача, переопределяющая настройки по умолчанию, запуск через переменную NODE_ENV=production
    "paths": {
      "assets": {
        "templates": "templates/"
      }
    },
    "minify": true
  }
  // Можно создать кастомные задачи с нужными параметрами, запуск по имени узла в json, вызов также как и с задачами выше, через переменную NODE_ENV=your_task
}
```


**.modernizrrc** 
можно добавить в корне проекта `.modernizrrc` файл, с настройками с официального сайта https://modernizr.com/ и получить доступ к modernizr с помощью `import modernizr from 'modernizr';`
```json
{
   "minify": true,
   "options": [
     "setClasses"
   ],
   "feature-detects": [
     "test/css/flexbox",
     "test/css/flexwrap",
     "test/css/vhunit",
     "test/css/vwunit"
   ]
 }
```

### Скрипты

  **Запуск линтера**
  ```
  npm run test
  ```
    
  **Запуск dev-cервера**
  ```
  npm run start
  ```
  При первом запуске будет создан конфигурационный файл
    
  **Запуск сборки для HtmlKodix** _Прим. NODE_ENV=`test`_
  ```
  npm run build
  npm run build-unix // для UNIX подобных систем
  ```
    
  **Запуск сборки для Production** _Прим. NODE_ENV=`production`_
  ```
  npm run deploy
  npm run deploy-unix // для UNIX подобных систем
  ```

  **Запуск сборок в режиме слежения**
  Например:
  ```
  npm run deploy:watch
  npm run build-unix:watch
  ```
  
  **Генерация CHANGELOG'a**
  ```
  npm run changelog
  ```
