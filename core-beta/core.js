/**
 * Created by UFS on 3/5/2017.
 * Fedor Ushakov <ushakovfserg@gmail.com>
 */



// Common packages
import fs from 'fs';
import path from 'path';
import yargs from 'yargs';

// Utility packages
import _ from 'lodash';
import util from 'util';
import glob from 'glob';

// Webpack
import webpack from 'webpack';
import {dllConfig, mainConfig} from './webpack';

// NodeEnvironment
import NODE_ENV from './environment.js';

// Additional scripts
import init from './init';

// Additional arguments
const argv = yargs.argv;

// Paths
const gitIgnorePath = path.resolve(process.cwd(), '.gitignore');
const packageJsonPath = path.resolve(process.cwd(), 'package.json');
const kxdConfigPath = path.resolve(process.cwd(), 'kdx.config.json');
const kdxConfigDefaultsPath = path.resolve(__dirname, 'defaults', 'kdxConfig.json');

// Default contents
let kdxConfigContents = JSON.parse(fs.readFileSync(kxdConfigPath, 'utf8'));
const kdxConfigDefaultsContents = JSON.parse(fs.readFileSync(kdxConfigDefaultsPath, 'utf8'));

// Setup part
// Create kdx.config if it not setup early
if (!fs.existsSync(kxdConfigPath)) {
    fs.writeFileSync(kxdConfigPath, JSON.stringify(kdxConfigDefaultsContents, null, 2));
} else {
    const updatedKdxConfigContents = _.defaultsDeep(kdxConfigContents, kdxConfigDefaultsContents);
    fs.writeFileSync(kxdConfigPath, JSON.stringify(updatedKdxConfigContents, null, 2));
    kdxConfigContents = updatedKdxConfigContents;
}

// Adding kdx.config to gitignore
if (fs.existsSync(gitIgnorePath)) {
    if (fs.readFileSync(gitIgnorePath, 'utf8').indexOf('\n\n# Kdx builder local config\nkdx.config.json') === -1) {
        console.log('.gitignore found... \nAppending');
        fs.appendFileSync(gitIgnorePath, '\n\n# Kdx builder local config\nkdx.config.json');
    }
} else {
    console.log('.gitignore NOT found... \nCreating');
    fs.writeFileSync(gitIgnorePath, '# Kdx builder local config\nkdx.config.json');
}

// Add or update scripts and devDependencies in package.json
(function () {
    const devDependeciesDefaultsPaths = path.resolve(__dirname, 'defaults', 'devDependecies.json');
    const scriptsDefaultsPaths = path.resolve(__dirname, 'defaults', 'scripts.json');


    const devDependencies = JSON.parse(fs.readFileSync(devDependeciesDefaultsPaths, 'utf8'));
    const scripts = JSON.parse(fs.readFileSync(scriptsDefaultsPaths, 'utf8'));

    let packageJsonContents = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));

    packageJsonContents.devDependencies = _.assign(packageJsonContents.devDependencies || {}, devDependencies);
    packageJsonContents.scripts = _.assign(packageJsonContents.scripts || {}, scripts);

    fs.writeFileSync(packageJsonPath, JSON.stringify(packageJsonContents, null, 2));
}());


// If it scaffold task break script task on finish
if (process.env.npm_lifecycle_event === 'scaffold') {
    init();
    console.log("\n\nDON'T FORGET TO RUN 'npm install' or 'yarn' NOW\n\n");
    process.exit();
}

// Getting config for certain environment
console.log('\nLaunching builder with settings for ' + NODE_ENV.toUpperCase() + ' environment.\n');
const CONFIG = _.defaultsDeep(kdxConfigContents[NODE_ENV], kdxConfigContents.default);

if (argv.debug || argv.showKdxConfig) {
    console.log('\nCompiled: KDX Config\n', util.inspect(CONFIG, false, null), '\n');
}


function webpackMain(CONFIG) {
    const mainWebpackConfig = mainConfig(CONFIG);

    if (argv.debug) {
        console.log('\nCompiled: Webpack MAIN Config\n', util.inspect(mainWebpackConfig, false, null), '\n');
        fs.writeFileSync(path.resolve(process.cwd(), 'debug.main.json'), JSON.stringify(mainWebpackConfig, null, 2));
    }
    const mainWebpackiInstance = webpack(mainWebpackConfig);
    if (CONFIG.devServer.active) {
        launchExpressServer(mainWebpackiInstance, mainWebpackConfig);
    } else {
        if (!argv.watch) {
            mainWebpackiInstance.run(function(err, stats) {
                if (err) {
                    console.log(err);
                    return;
                }
                if (NODE_ENV === 'production' || CONFIG.zip) {
                    (function() {
                        var zipdir = require('zip-dir');
                        var dateNow = new Date();
                        var tempDir = path.resolve('_temp/');

                        function prepDate(n) {
                            return (n < 10) ? '0' + n : n;
                        }


                        if (!fs.existsSync(tempDir)){
                            fs.mkdirSync(tempDir);
                        }

                        zipdir(CONFIG.paths.output, {
                            saveTo: path.resolve(tempDir, 'build_' + [prepDate(dateNow.getDate()), prepDate(dateNow.getMonth()+1), dateNow.getFullYear()].join('_') + '.zip')
                        }, function(err, buffer) {
                            if (err) {
                                console.log(err);
                                return;
                            }
                            return buffer;
                        });
                    })()
                }
            });
        } else {
            console.log('watching');
            mainWebpackiInstance.watch({ // watch options:
                aggregateTimeout: 300, // wait so long for more changes
                poll: true // use polling instead of native watchers
                // pass a number to set the polling interval
            }, function(err, stats) {
                if (err) {
                    console.log('webpack:build', err);
                }
                console.log(stats.toString({
                    chunks: false, // Makes the build much quieter
                    colors: true
                }))
            });
        }
    }

    function launchExpressServer(compiler, webpackConfig) {
        'use strict';

        const http = require('http');
        const express = require('express');
        const open = require('open');
        const app = express();

        (function initWebpack() {

            app.use(new require('webpack-dev-middleware')(compiler, {
                noInfo: true,
                publicPath: webpackConfig.output.publicPath,
                stats: {
                    colors: true,
                    chunks: false
                },
            }));


            app.use(require('webpack-hot-middleware')(compiler, {
                log: console.log,
                path: '/__webpack_hmr',
                heartBeat: 10 * 1000
            }));

            app.use(express.static(path.resolve(process.cwd(), CONFIG.paths.output)));
        })();


        var server = http.createServer(app);
        server.listen(CONFIG.devServer.port || 3001, function() {
            var address = server.address();
            console.log('Listening on: %j:%d', address, address.port);
        });

        // Open browser after server launch
        open('http://localhost:' + (CONFIG.devServer.port || 3001) + '/');
    }

}

if (CONFIG.libs.length) {
    const dllWebpackConfig = dllConfig(CONFIG);

    if (argv.debug) {
        console.log('\nCompiled: Webpack DLL Config\n', util.inspect(dllWebpackConfig, false, null), '\n');
        fs.writeFileSync(path.resolve(process.cwd(), 'debug.dll.json'), JSON.stringify(dllWebpackConfig, null, 2));
    }

    const webpackDllInstance = webpack(dllWebpackConfig);
    webpackDllInstance.run(function (err, stats) {
        if (err) {
            console.log(err);
            return;
        }

        webpackMain(CONFIG);
    });
} else {
    webpackMain(CONFIG);
}




