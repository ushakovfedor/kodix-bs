/**
 * Created by UFS on 3/5/2017.
 * Autoprefixer config file
 */

export default ["> 1%",
    "Firefox ESR",
    "last 2 versions",
    "Opera 12.1",
    "Android >= 2.3",
    "BlackBerry >= 7",
    "Chrome >= 9",
    "Firefox >= 4",
    "Explorer >= 9",
    "iOS >= 5",
    "Opera >= 11",
    "Safari >= 5",
    "OperaMobile >= 11",
    "OperaMini >= 6",
    "ChromeAndroid >= 9",
    "FirefoxAndroid >= 4",
    "ExplorerMobile >= 9"
];