/**
 * Created by UFS on 3/5/2017.
 *
 * Default settings for modernizr loader
 */

export default {
    "minify": true,
    "options": [
        "setClasses"
    ],
    "feature-detects": [
        "test/css/flexbox",
        "test/css/flexwrap",
        "test/css/vhunit",
        "test/css/vwunit"
    ]
}
