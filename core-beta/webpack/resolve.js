/*
 * Resolve config file
 * add pull request if you want to add common module to project
 */

import path from 'path';
import fs from 'fs';
import _ from 'lodash';

export default function (CONFIG) {
    return _.defaultsDeep(_.mapValues(CONFIG.paths.resolve, (resolvePath) => path.resolve(process.cwd(), resolvePath)), {
        "bourbon": path.join(require('bourbon').includePaths[0], "_bourbon.scss"),
        "bourbon-neat": path.join(require('bourbon-neat').includePaths[0], "_neat.scss"),
        "neat-helpers": path.join(require('bourbon-neat').includePaths[0], "_neat-helpers.scss"),
        "normalize-css": path.join(require('node-normalize-scss').includePaths, "_normalize.scss"),
        "reset-css": path.join(require('node-reset-scss').includePath, "_reset.scss"),
        modernizr$: (function () {
            const modernizrLocalRC = path.resolve(process.cwd(), ".modernizrrc");
            return fs.existsSync(modernizrLocalRC) ? modernizrLocalRC : path.resolve(__dirname, './modernizrrc.js');
        }())
    })
};
