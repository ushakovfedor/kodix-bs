/**
 * Created by UFS on 3/5/2017.
 */


// Common packages
import fs from 'fs';
import path from 'path';
import yargs from 'yargs';


const argv = yargs.argv;


// Utility packages
import _ from 'lodash';
import util from 'util';
import glob from 'glob';
import merge from 'webpack-merge';

// Webpack core
import webpack from 'webpack';


// Webpack constants
import NODE_ENV from './environment.js';

// Webpack parts
import webpackResole from './webpack/resolve.js';


// Webpack plugins
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import LiveReloadPlugin from 'webpack-livereload-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import AssetsPlugin from 'assets-webpack-plugin';
import OmitTildeWebpackPlugin from 'omit-tilde-webpack-plugin';

// PostCSS plugin
import autoprefixer from 'autoprefixer';
import autoprefixerConfig from './webpack/autoprefixer.js';

function scssProcessing(CONFIG) {
    const sassExtract = new ExtractTextPlugin({
        filename: CONFIG.paths.assets.css + '[name]' + (CONFIG.paths.hashes ? '.[contenthash]' : '') + '.css',
        allChunks: true,
        disable: CONFIG.devServer.active
    });

    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: sassExtract.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: !CONFIG.minify && !CONFIG.devServer.active,
                                    minimize: !CONFIG.devServer.active
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    plugins: [
                                        autoprefixer({browsers: autoprefixerConfig})
                                    ]
                                }
                            },
                            'resolve-url-loader',
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true,
                                    data: "$env: " + NODE_ENV + ";"
                                }
                            }
                        ]
                    })
                }
            ]
        },
        plugins: [
            sassExtract
        ]
    }

}

function commonLoaders(CONFIG) {
    // Loaders configuration
    let LOADERS = {
        module: {
            rules: _.flatten(_.map(glob.sync("./webpack/loaders/*.js", {
                cwd: __dirname
            }), function (filePath) {
                return require(filePath)(CONFIG);
            }))
        }
    };

    LOADERS = merge(LOADERS, scssProcessing(CONFIG));

    return LOADERS;
}

function commonResolve(CONFIG) {

    return {
        resolve: {
            extensions: ['.webpack.js', '.web.js', '.js', '.jsx', '.scss', '.ts'],
            alias: webpackResole(CONFIG)
        }
    };
}

function commonPlugins(CONFIG) {
    const PATHS = CONFIG.paths;
    const templatesPaths = glob.sync(PATHS.pages + '*/*.@(pug|html)', {
        cwd: process.cwd()
    });

    console.log(templatesPaths);
    let PLUGINS = [];

    if (CONFIG.libs.length) {
        PLUGINS.push(new webpack.DllReferencePlugin({
            context: process.cwd(),
            manifest: require(path.resolve(CONFIG.paths.output, 'dll-manifest.json'))
        }))
    } else {
        PLUGINS.push(
            new CleanWebpackPlugin(CONFIG.paths.output, {
                root: process.cwd(),
                verbose: true,
                dry: false
            }))
    }

    PLUGINS = _.concat(
        PLUGINS,
        _.map(templatesPaths, function (filepath) {
            const key = _.last(path.parse(filepath).dir.split('/'));
            return new HtmlWebpackPlugin({
                filename: CONFIG.paths.assets.templates + key + '.html',
                template: filepath,
                chunks: ['commons', key]
            })
        }),
        new webpack.DefinePlugin(_.assign({
            'process.env': {
                NODE_ENV: JSON.stringify(NODE_ENV),
                PAGES: JSON.stringify(
                    _.map(templatesPaths, function (filepath) {
                        return _.last(path.parse(filepath).dir.split('/'));
                    })
                ),
                PUBLIC_PATH: JSON.stringify(CONFIG.paths.publicPath)
            },
            KDX_TOOLS: CONFIG.kdxTools
        }, _.mapValues(CONFIG.provide, function (v) {
            return JSON.stringify(v);
        }))),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons',
            filename: CONFIG.paths.assets.js + 'commons' + (CONFIG.paths.hashes ? '.[hash]' : '') + '.js',
            minChunks: 2
        }),
        new AssetsPlugin({
            filename: 'assets.json',
            path: path.resolve(CONFIG.paths.output),
            prettyPrint: true
        })
    );

    if (CONFIG.devServer.active) {
        PLUGINS.push(new LiveReloadPlugin);
        PLUGINS.push(new webpack.HotModuleReplacementPlugin());
        PLUGINS.push(new webpack.NoEmitOnErrorsPlugin());
    }

    if (CONFIG.minify) {
        PLUGINS.push(new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            comments: false,
            compress: {
                drop_console: true
            }
        }));
    }

    return {
        plugins: PLUGINS
    }
}

function commonPart(CONFIG) {
    const PATHS = CONFIG.paths;

    // Get all entries in target path
    var entries = _.mapValues(_.keyBy(glob.sync(PATHS.pages + '*/*.@(js|ts)', {
        cwd: process.cwd()
    }), function (filePath) {
        var pathSplitted = filePath.split('/');
        return pathSplitted[pathSplitted.length - 2];
    }), function (entryPath) {
        return [
            "babel-polyfill",
            './' + entryPath
        ]
    });


    // Return undefined if no entries found
    if (Object.keys(entries).length == 0)
        return (void 0);


    // Add dev middleware if server is active
    if (CONFIG.devServer.active) {
        entries = _.mapValues(entries, function (entryPath) {
            return _.concat(
                entryPath,
                [
                    'webpack-hot-middleware/client?reload=true'
                ],
                fs.existsSync(path.resolve(process.cwd(), entryPath[1].replace('js', 'pug'))) ? [entryPath[1].replace('js', 'pug')] : []
            )
        });

        entries.commons = [
            'webpack-hot-middleware/client?reload=true'
        ]
    }

    if (CONFIG.kdxTools) {
        if (_.isArray(entries.commons)) {
            entries.commons.push(path.resolve(__dirname, './scripts/kdx-helper/kdx-helper.js'));
        }
        else {
            entries.commons = [path.resolve(__dirname, './scripts/kdx-helper/kdx-helper.js')];
        }
    }

    // Get all folders in resources/frontend for fast resolve
    var resolveFolders = _.mapValues(_.keyBy(glob.sync(PATHS.sourceRoot + '*/', {
        cwd: process.cwd()
    }), function (filePath) {
        var pathSplitted = filePath.split('/');
        return pathSplitted[pathSplitted.length - 2];
    }), function (filePath) {
        return path.join(process.cwd(), filePath);
    });

    return merge(
        {
            entry: entries,
            resolve: {
                alias: resolveFolders
            }
        },
        commonResolve(CONFIG),
        commonPlugins(CONFIG),
        commonLoaders(CONFIG)
    )
}

function developmentPart() {

}

function deploymentPart() {

}


export function dllConfig(CONFIG) {
    let WEBPACK_CONFIG = {
        entry: {
            dll: CONFIG.libs
        },
        output: {
            path: path.resolve(CONFIG.paths.output),
            publicPath: CONFIG.paths.publicPath,
            filename: 'bundle.[name].js',
            library: 'dll'
        },
        module: {
            rules: commonLoaders(CONFIG)
        },
        plugins: [
            new webpack.DllPlugin({
                name: 'dll',
                path: path.resolve(CONFIG.paths.output, '[name]-manifest.json')
            }),
            new AssetsPlugin({
                filename: 'assets-dlls.json',
                path: path.resolve(CONFIG.paths.output),
                prettyPrint: true
            }),
            new CleanWebpackPlugin(CONFIG.paths.output, {
                root: process.cwd(),
                verbose: true,
                dry: false
            })
        ],
        devtool: CONFIG.devServer.active ? 'cheap-module-eval-source-map' : false
    };

    WEBPACK_CONFIG = merge(WEBPACK_CONFIG, commonLoaders(CONFIG), commonResolve(CONFIG));

    return WEBPACK_CONFIG;
}

export function mainConfig(CONFIG) {
    const COMMON = commonPart(CONFIG);
    return COMMON;
}

