/**
 * Created by UFS on 3/5/2017.
 * Init script for project scaffold
 */
import fs from 'fs-extra';
import path from 'path';

export default function () {
    try {
        const src = path.resolve(__dirname, 'scaffold');
        const target = path.resolve(process.cwd());
        fs.copySync(src, target, {
            overwrite: false
        })
    } catch (err) {
        console.log('Folder scaffold error:\n' + err);
    }

}
