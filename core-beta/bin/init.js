/**
 * Created by UFS on 3/5/2017.
 * Init babel for core scripts
 */

var fs = require('fs');
var path = require('path');

var babelrc = fs.readFileSync(path.resolve(__dirname, '../../.babelrc'), 'utf8');
var config;

try {
    config = JSON.parse(babelrc);
} catch(err) {
    console.error('==>   ERROR: Error parsing .babelrc');
    console.error('err');
}

require('babel-core/register')(config);
require('../core');