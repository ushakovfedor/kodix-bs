/*
 * Pug loader config
 */
var path = require('path');

module.exports = function (CONFIG) {
  return {
    test: /\.pug/,
    loader: 'pug-loader',
    options: {
      pretty: !CONFIG.minify,
      root: path.resolve(process.cwd(), CONFIG.paths.sourceRoot)
    }
  }
};
