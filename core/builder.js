/**
 * Created by Kodix on 27.01.2017.
 * Fedor Ushakov <ushakovfserg@gmail.com>
 */

// Common packages
var fs = require('fs');
var path = require('path');
var argv = require('yargs').argv;


// Utility packages
var _ = require('lodash');
var util = require('util');
var glob = require('glob');

// Webpack
var webpack = require('webpack');

// NodeEnvironment
var NODE_ENV = require('./environment.config.js');


if (argv.update) {
  updatePkgJson();
  updateKdxConfigLocal();
  updateLaunchScript();
  return;
}


// Get or create builder config
var cnfPath = path.resolve(process.cwd(), 'kdx.config.json');
if (fs.existsSync(cnfPath)) {
  var CONFIG = JSON.parse(fs.readFileSync(cnfPath, 'utf8'));
} else {
  console.log('Initialization process...');

  // Creating config from template
  updateKdxConfigLocal()
  console.log('Default config created.');

  // Gitignore it
  var gitIgnorePath = path.resolve(process.cwd(), '.gitignore');
  if (fs.existsSync(gitIgnorePath)) {
    console.log('.gitignore found... \nAppending');
    fs.appendFileSync(gitIgnorePath, '\n\n# Kdx builder local config\nkdx.config.json');
  } else {
    console.log('.gitignore NOT found... \nCreating');
    fs.writeFileSync(gitIgnorePath, '# Kdx builder local config\nkdx.config.json');
  }


  updateLaunchScript();

  // Add package.json default scripts
  updatePkgJson();

  console.log('Setup kdx.config.json for test(html.kodix.ru deploy) and production.\nEvery environment setup EXTENDS default.');
  return;
}


// Getting config for certain environment
console.log('\nLaunching builder with settings for ' + NODE_ENV.toUpperCase() + ' environment.\n');
CONFIG = _.defaultsDeep(CONFIG[NODE_ENV], CONFIG.default);

if (argv.debug || argv.showKdxConfig)
  console.log('\nCompiled: KDX Config\n', util.inspect(CONFIG, false, null), '\n');

var WEBPACK_CONFIG = require('./webpack.config.js')(CONFIG);

if (typeof WEBPACK_CONFIG === "undefined") {
  console.log('\n\nCritical: Not found any entry points for Webpack.\n-----------\n\tCheck paths.pages routes or add new page\n-----------');
  return;
}

if (argv.debug)
  console.log('\nCompiled: Webpack Config\n', util.inspect(WEBPACK_CONFIG, false, null), '\n');


var WEBPACK = webpack(WEBPACK_CONFIG);

if (CONFIG.devServer.active) {
  launchExpressServer(WEBPACK, WEBPACK_CONFIG);
} else {
  if (!argv.watch) {
    WEBPACK.run(function(err, stats) {
      if (err) {
        console.log(err);
        return;
      }
      if (NODE_ENV === 'production' || CONFIG.zip) {
        (function() {
          var zipdir = require('zip-dir');
          var dateNow = new Date();
          var tempDir = path.resolve('_temp/');

          function prepDate(n) {
            return (n < 10) ? '0' + n : n;
          }


          if (!fs.existsSync(tempDir)){
              fs.mkdirSync(tempDir);
          }

          zipdir(CONFIG.paths.output, {
            saveTo: path.resolve(tempDir, 'build_' + [prepDate(dateNow.getDate()), prepDate(dateNow.getMonth()+1), dateNow.getFullYear()].join('_') + '.zip')
          }, function(err, buffer) {
            if (err) {
              console.log(err);
              return;
            }
            return buffer;
          });
        })()
      }
    });
  } else {
    console.log('watching');
    WEBPACK.watch({ // watch options:
      aggregateTimeout: 300, // wait so long for more changes
      poll: true // use polling instead of native watchers
      // pass a number to set the polling interval
    }, function(err, stats) {
      if (err) {
        console.log('webpack:build', err);
      }
      console.log(stats.toString({
        chunks: false, // Makes the build much quieter
        colors: true
      }))
    });
  }
}

function launchExpressServer(compiler, webpackConfig) {
  'use strict';

  var http = require('http');
  var express = require('express');
  var open = require('open');
  var app = express();

  (function initWebpack() {

    app.use(new require('webpack-dev-middleware')(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath,
      stats: {
        colors: true,
        chunks: false
      },
    }));


    app.use(require('webpack-hot-middleware')(compiler, {
      log: console.log,
      path: '/__webpack_hmr',
      heartBeat: 10 * 1000
    }));

    app.use(express.static(path.resolve(process.cwd(), CONFIG.paths.output)));
  })();


  var server = http.createServer(app);
  server.listen(CONFIG.devServer.port || 3001, function() {
    var address = server.address();
    console.log('Listening on: %j:%d', address, address.port);
  });

  // Open browser after server launch
  open('http://localhost:' + (CONFIG.devServer.port || 3001) + '/');
}


function updateLaunchScript() {
  // Create kdx.builder.js script
  console.log('Updating kdx.builder.js');
  var kdxScriptPaths = path.resolve(process.cwd(), 'kdx.build.js');
  if (!fs.existsSync(kdxScriptPaths) || argv.update) {
    var kdxScriptFileContents = fs.readFileSync(path.resolve(__dirname, './local/kdx.builder.default.js'), 'utf8');
    if (path.resolve(__dirname, '../') == process.cwd()) {
      kdxScriptFileContents = kdxScriptFileContents.replace('kdx-builder', './index.js');
    }
    fs.writeFileSync(path.resolve(process.cwd(), 'kdx.builder.js'), kdxScriptFileContents);
  }
}

function updateKdxConfigLocal() {
  console.log('Updating kdx.config.js');
  var cnfPath = path.resolve(process.cwd(), 'kdx.config.json');
  var cnfTemplate = require('./../core-beta/defaults/kdx.config.default.json');

  if(fs.existsSync(cnfPath)) {
    var cnfCurrent = require(cnfPath);
    cnfTemplate = _.defaultsDeep(cnfCurrent, cnfTemplate);
  }

  fs.writeFileSync(cnfPath, JSON.stringify(cnfTemplate, null, 2));
}

function updatePkgJson() {
  var pkgJsonPath = path.resolve(process.cwd(), 'package.json');
  var pkgJsonObject = JSON.parse(fs.readFileSync(pkgJsonPath, 'utf8'));
  console.log('Updating package.json');

  // Updating dev dependencies
  pkgJsonObject.devDependencies = _.assign(pkgJsonObject.devDependencies || {}, require('./local/dependencies.default.json'));

  // Updating scripts
  pkgJsonObject.scripts = _.assign(pkgJsonObject.scripts || {}, require('./local/scripts.default.json'));

  // Write updated file
  fs.writeFileSync(pkgJsonPath, JSON.stringify(pkgJsonObject, null, 2));
  console.log('Updated package.json');
}
